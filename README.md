# React Navigation

## (a)

### 1.install app center packages

```bash
npm install @react-navigation/native
npm install @react-navigation/native-stack
npm install react-native-screens react-native-safe-area-context
npm install native-base
cd ios
arch -x86_64 pod install
cd ..
```

### 2.react-native-screens

```bash
cd android/app/src/main/java/<your package name>/MainActivity.java.
```

![](./docs/img/MainActivity.png)

```java
import android.os.Bundle;

@Override
protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(null);
}
```

![](./docs/img/MainActivityCode.png)

### 3.Wrapping your app in NavigationContainer​

```java
import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import {NavigationContainer} from '@react-navigation/native';

const RNRedux = () => (
  <NavigationContainer>
    <App />
  </NavigationContainer>
);

AppRegistry.registerComponent(appName, () => RNRedux);
```

![](./docs/img/NavigationContainer.png)

### 4. Creating a native stack navigator​ && Navigating to a new screen​

create folder

```bash
open folder
src > navigation > Router.js
src > screen > Home > Home.js
src > screen > Login > Login.js
src > screen > Login > Register.js
src > screen > Login > ConfirmEmail.js
src > screen > Login > ForgotPassword.js
src > screen > Login > ResetPassword.js
src > screen > Login > components > CustomInput.js
src > screen > Login > components > CustomButton.js
src > screen > Login > components > Header.js
src > screen > Login > components > SocialButton.js
```

App.js

```java
import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from './src/Screens/Login/Login';
import Register from './src/Screens/Login/Register';
import ForgotPassword from './src/Screens/Login/ForgotPassword';
import ResetPassword from './src/Screens/Login/ResetPassword';
import ConfirmEmail from './src/Screens/Login/ConfirmEmail';
import TodoApp from './src/Screens/Todo/TodoPage';

const Stack = createNativeStackNavigator();
const Routes = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Forgot Password" component={ForgotPassword} />
      <Stack.Screen name="Reset Password" component={ResetPassword} />
      <Stack.Screen name="Confirm Email" component={ConfirmEmail} />
      <Stack.Screen name="Todo App" component={TodoApp} />
    </Stack.Navigator>
  );
};
function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;


```

Login.js

```java
import {Box, Image, ScrollView} from 'native-base';
import React, {useState} from 'react';
import Header from './Header';
import CustomInput from './CustomInput';
import CustomButton from './CustomButton';
import SocialButton from './SocialButton';
import {useNavigation} from '@react-navigation/native';

function Login() {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();
  const onLoginPressed = () => {
    console.warn('Login');
    navigation.navigate('Todo App'); //todo app as home page after login
  };
  const onForgotPasswordPressed = () => {
    console.warn('Forgot Password');
    navigation.navigate('Forgot Password');
  };

  const onRegisterPressed = () => {
    console.warn('Register');
    navigation.navigate('Register');
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Box>
        <Header title="Login" />
        <Image
          borderRadius={100}
          source={{
            uri: 'https://reactjs.org/logo-og.png',
          }}
          alt="logo"
          h="90px"
          w="90px"
          m="10"
          alignSelf="center"
        />

        <Box mx="5">
          <CustomInput
            placeholder="Username"
            text="user"
            value={name}
            setValue={setName}
          />
          <CustomInput
            placeholder="Password"
            text="lock"
            value={password}
            setValue={setPassword}
            secureTextEntry={true}
          />

          <CustomButton text="Login" onPress={onLoginPressed} />
          <CustomButton
            text="Forgot password?"
            onPress={onForgotPasswordPressed}
            type="tertiary"
          />
          <SocialButton />
          <CustomButton
            text="New User? Register here."
            onPress={onRegisterPressed}
            type="tertiary" //button style id different than other
          />
        </Box>
      </Box>
    </ScrollView>
  );
}
export default Login;


```

CustomButton.js

```java
import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {Button, Pressable} from 'native-base';


//set button type primary as default button style
const CustomButton = ({text, onPress, bgColor, fgColor, type = 'primary'}) => {
  return (
    <View>
      <Pressable
        onPress={onPress}
        style={[
          styles.container,
          styles[`container_${type}`],
          bgColor ? {backgroundColor: bgColor} : {},
        ]}>
        <Text
          style={[
            styles.text,
            styles[`text_${type}`],
            fgColor ? {color: fgColor} : {},
          ]}>
          {text}
        </Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 5,
  },
  //create button type
  container_primary: {
    backgroundColor: '#3b71f3',
  },
  container_tertiary: {
    backgroundColor: '',
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
  text_tertiary: {
    color: 'gray',
  },
});
export default CustomButton;

```

### - Validation have not set. Only the basic code with buttons which can be pressed.

### - Write the rest of the code in the similar way.
