import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from './src/Screens/Login/Login';
import Register from './src/Screens/Login/Register';
import ForgotPassword from './src/Screens/Login/ForgotPassword';
import ResetPassword from './src/Screens/Login/ResetPassword';
import ConfirmEmail from './src/Screens/Login/ConfirmEmail';
import TodoApp from './src/Screens/Todo/TodoPage';

const Stack = createNativeStackNavigator();
const Routes = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Register" component={Register} />
      <Stack.Screen name="Forgot Password" component={ForgotPassword} />
      <Stack.Screen name="Reset Password" component={ResetPassword} />
      <Stack.Screen name="Confirm Email" component={ConfirmEmail} />
      <Stack.Screen name="Todo App" component={TodoApp} />
    </Stack.Navigator>
  );
};
function App() {
  return (
    <>
      <Routes />
    </>
  );
}

export default App;
