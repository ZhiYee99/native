import {Box, Button, Image, Input, ScrollView, Text, View} from 'native-base';
import React, {useState} from 'react';
import {Logo} from '../../../assets/images/logo.png';
import {Alert} from 'react-native';
import CustomButton from './components/CustomButton';
import CustomInput from './components/CustomInput';
import {useNavigation} from '@react-navigation/native';

function ResetPassword() {
  const [confirmCode, setConfirmCode] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();
  const onLoginPressed = () => {
    console.warn('Back to Sign in');
    navigation.navigate('Login');
  };
  const onSubmitPressed = () => {
    Alert.alert('New password is set successfully.')
    navigation.navigate('Login');
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Box>
        <Text alignSelf="center" fontWeight="bold" fontSize="2xl" p="10">
          Reset Your Password
        </Text>
        <Box mx="5">
          <Text>Enter your confirmation code *</Text>
          <CustomInput
            placeholder="Enter your confirmation code"
            value={confirmCode}
            setValue={setConfirmCode}
          />
          <Text>Password *</Text>
          <CustomInput
            placeholder="Enter your new password"
            value={password}
            setValue={setPassword}
          />
          <CustomButton
            text="Submit"
            onPress={onSubmitPressed}
            bgColor="#FFD27F"
          />
          <CustomButton
            text="Back to Sign In"
            onPress={onLoginPressed}
            type="tertiary"
            bgColor="white"
            fgColor="#FFD27F"
          />
        </Box>
      </Box>
    </ScrollView>
  );
}

export default ResetPassword;
