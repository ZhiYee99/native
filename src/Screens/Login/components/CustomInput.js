import {View, Text, StyleSheet, TextInput} from 'react-native';
import React from 'react';
import {Icon, Input, SunIcon} from 'native-base';
import {AiOutlineUser} from 'react-icons/ai';

const CustomInput = ({placeholder, text, value, setValue, secureTextEntry}) => {
  return (
    <View style={styles.inputContainer}>
      <Icon text={text} bgColor="#ddd" size={5} />
      <TextInput
        value={value}
        onChange={setValue}
        placeholder={placeholder}
        style={styles.input}
        secureTextEntry={secureTextEntry}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#bbb',
    margin: 10,
  },
  input: {
    flex: 1,
    marginVertical: 2,
    padding: 5,
    paddingLeft: 10,
  },
});
export default CustomInput;
