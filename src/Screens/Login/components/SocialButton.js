import {View, Text} from 'react-native';
import React from 'react';
import CustomButton from './CustomButton';

const SocialButton = () => {
  const onFBPressed = () => {
    console.warn('FB');
  };
  const onGooglePressed = () => {
    console.warn('Google');
  };
  const onApplePressed = () => {
    console.warn('Apple');
  };
  return (
    <View>
      <CustomButton
        text="Sign in with Facebook"
        bgColor="#E7EAF4"
        fgColor="#4765A9"
        onPress={onFBPressed}
      />
      <CustomButton
        text="Sign in with Google"
        bgColor="#FAE9EA"
        fgColor="#DD4D44"
        onPress={onGooglePressed}
      />
      <CustomButton
        text="Sign in with Apple"
        bgColor="#E3E3E3"
        fgColor="#363636"
        onPress={onApplePressed}
      />
    </View>
  );
};

export default SocialButton;
