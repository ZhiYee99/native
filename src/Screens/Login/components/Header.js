import {Box, Text} from 'native-base';
import React from 'react';
import {StyleSheet} from 'react-native';

function Header({title}) {
  return (
    <Box style={styles.header}>
      <Text style={styles.title}>{title}</Text>
    </Box>
  );
}
const styles = StyleSheet.create({
  header: {
    height: 80,
    paddingTop: 38,
    backgroundColor: '#73B0ED',
  },
  title: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
export default Header;
