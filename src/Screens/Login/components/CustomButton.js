import {View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {Button, Pressable} from 'native-base';

const CustomButton = ({text, onPress, bgColor, fgColor, type = 'primary'}) => {
  return (
    <View>
      {/* <Button mt="5" bgColor={bgColor} onPress={onPress} color={color}>
        {text}
      </Button> */}
      <Pressable
        onPress={onPress}
        style={[
          styles.container,
          styles[`container_${type}`],
          bgColor ? {backgroundColor: bgColor} : {},
        ]}>
        <Text
          style={[
            styles.text,
            styles[`text_${type}`],
            fgColor ? {color: fgColor} : {},
          ]}>
          {text}
        </Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 15,
    marginVertical: 5,
    alignItems: 'center',
    borderRadius: 5,
  },
  container_primary: {
    backgroundColor: '#3b71f3',
  },
  container_tertiary: {
    backgroundColor: '',
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
  },
  text_tertiary: {
    color: 'gray',
  },
});
export default CustomButton;
