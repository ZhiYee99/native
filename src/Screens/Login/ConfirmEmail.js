import {Box, Button, Image, Input, ScrollView, Text, View} from 'native-base';
import React, {useState} from 'react';
import {Logo} from '../../../assets/images/logo.png';
import {Alert} from 'react-native';
import CustomButton from './components/CustomButton';
import CustomInput from './components/CustomInput';
import {useNavigation} from '@react-navigation/native';

function ConfirmEmail() {
  const [email, setEmail] = useState('');
  const [confirmCode, setConfirmCode] = useState('');
  const navigation = useNavigation();
  const onConfirmPressed = () => {
    console.warn('Register successfully.');
    Alert.alert('Register successfully.');
    navigation.navigate('Login');
  };
  const onLoginPressed = () => {
    console.warn('Back to Sign in');
    navigation.navigate('Login');
  };
  const onResendCodePressed = () => {
    Alert.alert('Code is resent. Please check your email.')
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Box>
        <Text alignSelf="center" fontWeight="bold" fontSize="2xl" p="10">
          Confirm Your Email
        </Text>
        <Box mx="5">
          <Text>Enter your email *</Text>
          <CustomInput
            placeholder="Enter your email"
            value={email}
            setValue={setEmail}
          />
          <Text>Enter your confirmation code *</Text>
          <CustomInput
            placeholder="Enter your confirmation code"
            value={confirmCode}
            setValue={setConfirmCode}
          />

          <CustomButton
            text="Confirm"
            onPress={onConfirmPressed}
            bgColor="#FFD27F"
          />
          <View flexDirection="row" justifyContent="space-evenly">
            <CustomButton
              text="Resend Code"
              onPress={onResendCodePressed}
              type="tertiary"
              bgColor="white"
              fgColor="#FFD27F"
            />
            <CustomButton
              text="Back to Sign In"
              onPress={onLoginPressed}
              type="tertiary"
              bgColor="white"
              fgColor="#FFD27F"
            />
          </View>
        </Box>
      </Box>
    </ScrollView>
  );
}

export default ConfirmEmail;
