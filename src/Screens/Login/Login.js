import {Box, Image, ScrollView} from 'native-base';
import React, {useState} from 'react';
import Header from './components/Header';
import CustomInput from './components/CustomInput';
import CustomButton from './components/CustomButton';
import SocialButton from './components/SocialButton';
import {useNavigation} from '@react-navigation/native';

function Login() {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const navigation = useNavigation();
  const onLoginPressed = () => {
    console.warn('Login');
    navigation.navigate('Todo App');
  };
  const onForgotPasswordPressed = () => {
    console.warn('Forgot Password');
    navigation.navigate('Forgot Password');
  };

  const onRegisterPressed = () => {
    console.warn('Register');
    navigation.navigate('Register');
  };
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Box>
        <Header title="Login" />
        <Image
          borderRadius={100}
          source={{
            uri: 'https://reactjs.org/logo-og.png',
          }}
          alt="logo"
          h="90px"
          w="90px"
          m="10"
          alignSelf="center"
        />

        <Box mx="5">
          <CustomInput
            placeholder="Username"
            text="user"
            value={name}
            setValue={setName}
          />
          <CustomInput
            placeholder="Password"
            text="lock"
            value={password}
            setValue={setPassword}
            secureTextEntry={true}
          />

          <CustomButton text="Login" onPress={onLoginPressed} />
          <CustomButton
            text="Forgot password?"
            onPress={onForgotPasswordPressed}
            type="tertiary"
          />
          <SocialButton />
          <CustomButton
            text="New User? Register here."
            onPress={onRegisterPressed}
            type="tertiary"
          />
        </Box>
      </Box>
    </ScrollView>
  );
}
export default Login;
