import {Box, Button, Image, Input, ScrollView, Text, View} from 'native-base';
import React, {useState} from 'react';
import {Logo} from '../../../assets/images/logo.png';
import {Alert} from 'react-native';
import CustomButton from './components/CustomButton';
import CustomInput from './components/CustomInput';
import {useNavigation} from '@react-navigation/native';

function ForgotPassword() {
  const [name, setName] = useState('');
  const navigation = useNavigation();
  const onSendPressed = () => {
    console.warn('Send username');
    navigation.navigate('Reset Password');
  };
  const onLoginPressed = () => {
    console.warn('Back to Sign in');
    navigation.navigate('Login');
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Box>
        <Text alignSelf="center" fontWeight="bold" fontSize="2xl" p="10">
          Reset Your Password
        </Text>
        <Box mx="5">
          <Text>Enter your username *</Text>
          <CustomInput
            placeholder="Enter your username"
            value={name}
            setValue={setName}
          />

          <CustomButton text="Send" onPress={onSendPressed} bgColor="#FFD27F" />
          <CustomButton
            text="Back to Sign In"
            onPress={onLoginPressed}
            type="tertiary"
            bgColor="white"
            fgColor="#FFD27F"
          />
        </Box>
      </Box>
    </ScrollView>
  );
}

export default ForgotPassword;
