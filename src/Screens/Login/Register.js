import {Box, Button, Image, Input, ScrollView, Text} from 'native-base';
import React, {useState} from 'react';
import Header from './components/Header';
import {Logo} from '../../../assets/images/logo.png';
import {Alert} from 'react-native';
import CustomButton from './components/CustomButton';
import CustomInput from './components/CustomInput';
import SocialButton from './components/SocialButton';
import {useNavigation} from '@react-navigation/native'

function Register() {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const navigation = useNavigation();
  const onConfirmPressed = () => {
    console.warn('Confirm');
    navigation.navigate('Login')
  };
  const onRegisterPressed = () => {
    console.warn('Register');
    navigation.navigate('Confirm Email')
  };
  
  const onTermsOfUsePressed = ()=>{
    console.warn('Terms of use')
  }
  const onPrivacyPolicyPressed = ()=>{
    console.warn('Privacy Policy')
  }
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <Box>
        <Header title="Register" />
        {/* <Image
          // source={Logo}
          source={{
            uri: 'https://reactjs.org/logo-og.png',
          }}
          alt="logo"
          h="90px"
          w="90px"
          m="10"
          alignSelf="center"
          borderRadius={100}
        /> */}
        <Text alignSelf="center" fontWeight="bold" fontSize="2xl" p="10">
          Create an account
        </Text>
        <Box mx="5">
          <CustomInput placeholder="Username" value={name} setValue={setName} />
          <CustomInput placeholder="Email" value={email} setValue={setEmail} />
          <CustomInput
            placeholder="Password"
            value={password}
            setValue={setPassword}
            secureTextEntry={true}
          />
          <CustomInput
            placeholder="Confirm Password"
            value={confirmPassword}
            setValue={setConfirmPassword}
            secureTextEntry={true}
          />

          <CustomButton text="Register" onPress={onRegisterPressed} />
          <Text my="2">
            By registering, you confirm that you accept our
            <Text color="#fdb875" onPress={onTermsOfUsePressed}> Terms of Use </Text> and
            <Text color="#fdb875" onPress={onPrivacyPolicyPressed}> Privacy Policy </Text>.
          </Text>
        <SocialButton/>
          <CustomButton
            text="Have an account? Login here."
            type="tertiary"
            onPress={onConfirmPressed}
          />
        </Box>
      </Box>
    </ScrollView>
  );
}

export default Register;
