import React, {useState} from 'react';
import {Button, Input, View} from 'native-base';
import {StyleSheet, TextInput} from 'react-native';

export default function AddTodo({submitHandler}) {
  const [text, setText] = useState('');

  const changeHandler = val => {
    setText(val);
  };

  return (
    <View>
      <Input
        style={styles.input}
        placeholder="new todo..."
        onChangeText={changeHandler}
        value={text}
      />

      <Button colorScheme="blue" onPress={() => submitHandler(text)} mt="5">
        Add todo
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    padding: 16,
  },
});
