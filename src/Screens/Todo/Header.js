import React from 'react';
import {Box, Text, View} from 'native-base';
import {StyleSheet} from 'react-native';

function Header() {
  return (
    <Box style={styles.header} >
      <Text style={styles.title}>Todo List</Text>
    </Box>
  );
}

const styles = StyleSheet.create({
  header: {
    height: 80,
    paddingTop: 38,
    backgroundColor: 'coral',
  },
  title: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default Header;
