import React, {useState} from 'react';
import {Box, FlatList,} from 'native-base';
import {
  Alert,
  Keyboard,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';
import Header from './Header';
import AddTodo from './AddTodo';
import TodoItem from './TodoItem';

export default function TodoPage() {
  const [todos, setTodos] = useState([
    {text: 'buy coffee', key: '1'},
    {text: 'create an app', key: '2'},
    {text: 'play on the switch', key: '3'},
  ]);

  const pressHandler = key => {
    setTodos(prevTodos => {
      return prevTodos.filter(todo => todo.key != key);
    });
  };

  const submitHandler = text => {
    if (text.length > 5) {
      // setText('');
      setTodos(prevTodos => {
        return [{text, key: Math.random().toString()}, ...prevTodos];
      });
    } else {
      Alert.alert('OOPS', 'Todo must be over 5 characters long', [
        {text: 'Understood', onPress: () => console.log('alert closed')},
      ]);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <Box style={styles.container}>
        <Header />
        <Box style={styles.content}>
          <AddTodo submitHandler={submitHandler} />
          <Box style={styles.list}>
            <FlatList
              data={todos}
              renderItem={({item}) => (
                <TodoItem item={item} pressHandler={pressHandler} />
              )}
            />
          </Box>
        </Box>
      </Box>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  content: {
    padding: 40,
  },
  list: {
    marginTop: 20,
  },
});
