import React from 'react';
import {Text} from 'native-base';
import {StyleSheet, TouchableOpacity} from 'react-native';

function TodoItem({item, pressHandler}) {
  return (
    <TouchableOpacity
      onPress={() => {
        pressHandler(item.key);
      }}>
      <Text style={styles.item}>{item.text}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  item: {
    padding: 16,
    marginTop: 16,
    borderColor: '#777',
    borderWidth: 1,
    borderRadius: 10,
  },
});

export default TodoItem;
