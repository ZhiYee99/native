/**
 * @format
 */

import {NavigationContainer} from '@react-navigation/native';
import {NativeBaseProvider} from 'native-base';
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

const RNRedux = () => {
  return (
    <NavigationContainer>
      <NativeBaseProvider>
        <App />
      </NativeBaseProvider>
    </NavigationContainer>
  );
};
AppRegistry.registerComponent(appName, () => RNRedux);
